// Copyright (C) 2022 Michael Mestnik <cheako@mikemestnik.net>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#![allow(unused_imports)]

use ash::vk;
use either::Either;
use std::ffi::{self, c_void, CStr, CString};
use std::ptr;
use std::slice;

trait MyClone {
    fn my_clone(&self) -> Self;
    fn my_drop(&self);
}

trait MySliceClone {
    fn my_slice_clone(&self, count: u32) -> Self;
    fn my_slice_drop(&self, count: u32);
}

impl MyClone for *const vk::SpecializationInfo {
    fn my_clone(&self) -> Self {
        if self.is_null() {
            return ptr::null();
        }
        let mut ret = Box::new(unsafe { self.read() });
        ret.p_map_entries = Box::<[_]>::leak(
            unsafe { slice::from_raw_parts(ret.p_map_entries, ret.map_entry_count as _) }.into(),
        )
        .as_ptr();
        ret.p_data = Box::<[_]>::leak(
            unsafe { slice::from_raw_parts(ret.p_data as *const u8, ret.data_size as _) }.into(),
        )
        .as_ptr() as _;
        Box::leak(ret)
    }

    fn my_drop(&self) {
        if self.is_null() {
            return;
        }
        let this = unsafe { Box::from_raw(*self as *mut vk::SpecializationInfo) };
        unsafe {
            Box::from_raw(ptr::from_raw_parts::<[vk::SpecializationMapEntry]>(
                this.p_map_entries as *const (),
                this.map_entry_count as _,
            ) as *mut [vk::SpecializationMapEntry])
        };
        unsafe {
            Box::from_raw(
                ptr::from_raw_parts::<[u8]>(this.p_data as *const (), this.data_size as _)
                    as *mut [u8],
            )
        };
    }
}

impl MySliceClone for *const vk::PipelineShaderStageCreateInfo {
    fn my_slice_clone(&self, count: u32) -> Self {
        if self.is_null() || count == 0 {
            return ptr::null();
        }
        let mut ret: Box<[_]> = unsafe { slice::from_raw_parts(*self, count as _) }.into();
        for this in ret.iter_mut() {
            assert!(this.p_next.is_null());
            this.p_name = if this.p_name.is_null() {
                ptr::null()
            } else {
                Box::leak(
                    ffi::CString::from(unsafe { ffi::CStr::from_ptr(this.p_name) })
                        .into_boxed_c_str(),
                )
                .as_ptr()
            };
            this.p_specialization_info = this.p_specialization_info.my_clone();
        }
        Box::leak(ret).as_ptr()
    }

    fn my_slice_drop(&self, count: u32) {
        if self.is_null() || count == 0 {
            return;
        }
        let mut this = unsafe {
            Box::from_raw(ptr::from_raw_parts::<[u8]>(*self as *const (), count as _)
                as *mut [vk::PipelineShaderStageCreateInfo])
        };
        for this in this.iter_mut() {
            unsafe {
                Box::from_raw(ptr::from_raw_parts::<CStr>(
                    this.p_name as *const (),
                    CStr::from_ptr(this.p_name).to_bytes_with_nul().len(),
                ) as *mut ffi::CStr);
            }
            this.p_specialization_info.my_drop();
        }
    }
}

impl MyClone for *const vk::PipelineVertexInputStateCreateInfo {
    fn my_clone(&self) -> Self {
        if self.is_null() {
            return ptr::null();
        }
        let mut ret = Box::new(unsafe { self.read() });
        assert!(ret.p_next.is_null());
        ret.p_vertex_binding_descriptions = if ret.p_vertex_binding_descriptions.is_null()
            || ret.vertex_binding_description_count == 0
        {
            ptr::null()
        } else {
            Box::<[_]>::leak(
                (*unsafe {
                    slice::from_raw_parts(
                        ret.p_vertex_binding_descriptions,
                        ret.vertex_binding_description_count as _,
                    )
                })
                .into(),
            )
            .as_ptr()
        };
        ret.p_vertex_attribute_descriptions = if ret.p_vertex_attribute_descriptions.is_null()
            || ret.vertex_attribute_description_count == 0
        {
            ptr::null()
        } else {
            Box::<[_]>::leak(
                (*unsafe {
                    slice::from_raw_parts(
                        ret.p_vertex_attribute_descriptions,
                        ret.vertex_attribute_description_count as _,
                    )
                })
                .into(),
            )
            .as_ptr()
        };
        Box::leak(ret)
    }

    fn my_drop(&self) {
        if self.is_null() {
            return;
        }
        let this = unsafe { Box::from_raw(*self as *mut vk::PipelineVertexInputStateCreateInfo) };
        if !this.p_vertex_binding_descriptions.is_null()
            && this.vertex_binding_description_count != 0
        {
            unsafe {
                Box::from_raw(ptr::from_raw_parts::<[vk::VertexInputBindingDescription]>(
                    this.p_vertex_binding_descriptions as *const (),
                    this.vertex_binding_description_count as _,
                )
                    as *mut [vk::PipelineShaderStageCreateInfo]);
            }
        }
        if !this.p_vertex_attribute_descriptions.is_null()
            && this.vertex_attribute_description_count != 0
        {
            unsafe {
                Box::from_raw(
                    ptr::from_raw_parts::<[vk::VertexInputAttributeDescription]>(
                        this.p_vertex_attribute_descriptions as *const (),
                        this.vertex_attribute_description_count as _,
                    ) as *mut [vk::PipelineShaderStageCreateInfo],
                );
            }
        }
    }
}

impl MyClone for *const vk::PipelineRasterizationStateCreateInfo {
    fn my_clone(&self) -> Self {
        if self.is_null() {
            return ptr::null();
        }
        let ret = Box::new(unsafe { self.read() });
        assert!(ret.p_next.is_null());
        Box::leak(ret)
    }

    fn my_drop(&self) {
        if self.is_null() {
            return;
        }
        unsafe { Box::from_raw(*self as *mut vk::PipelineRasterizationStateCreateInfo) };
    }
}

impl MyClone for *const vk::PipelineTessellationStateCreateInfo {
    fn my_clone(&self) -> Self {
        if self.is_null() {
            return ptr::null();
        }
        let ret = Box::new(unsafe { self.read() });
        assert!(ret.p_next.is_null());
        Box::leak(ret)
    }

    fn my_drop(&self) {
        if self.is_null() {
            return;
        }
        unsafe { Box::from_raw(*self as *mut vk::PipelineTessellationStateCreateInfo) };
    }
}

impl MyClone for *const vk::PipelineViewportStateCreateInfo {
    fn my_clone(&self) -> Self {
        if self.is_null() {
            return ptr::null();
        }
        let mut ret = Box::new(unsafe { self.read() });
        assert!(ret.p_next.is_null());
        ret.p_viewports = if ret.p_viewports.is_null() || ret.viewport_count == 0 {
            ptr::null()
        } else {
            Box::<[_]>::leak(
                (*unsafe { slice::from_raw_parts(ret.p_viewports, ret.viewport_count as _) })
                    .into(),
            )
            .as_ptr()
        };
        ret.p_scissors = if ret.p_scissors.is_null() || ret.scissor_count == 0 {
            ptr::null()
        } else {
            Box::<[_]>::leak(
                (*unsafe { slice::from_raw_parts(ret.p_scissors, ret.scissor_count as _) }).into(),
            )
            .as_ptr()
        };
        Box::leak(ret)
    }

    fn my_drop(&self) {
        if self.is_null() {
            return;
        }
        let this = unsafe { Box::from_raw(*self as *mut vk::PipelineViewportStateCreateInfo) };
        if !this.p_viewports.is_null() && this.viewport_count != 0 {
            unsafe {
                Box::from_raw(ptr::from_raw_parts::<[vk::VertexInputBindingDescription]>(
                    this.p_viewports as *const (),
                    this.viewport_count as _,
                )
                    as *mut [vk::PipelineShaderStageCreateInfo]);
            }
        }
        if !this.p_scissors.is_null() && this.scissor_count != 0 {
            unsafe {
                Box::from_raw(
                    ptr::from_raw_parts::<[vk::VertexInputAttributeDescription]>(
                        this.p_scissors as *const (),
                        this.scissor_count as _,
                    ) as *mut [vk::PipelineShaderStageCreateInfo],
                );
            }
        }
    }
}

impl MyClone for *const vk::PipelineInputAssemblyStateCreateInfo {
    fn my_clone(&self) -> Self {
        if self.is_null() {
            return ptr::null();
        }
        let ret = Box::new(unsafe { self.read() });
        assert!(ret.p_next.is_null());
        Box::leak(ret)
    }

    fn my_drop(&self) {
        if self.is_null() {
            return;
        }
        unsafe { Box::from_raw(*self as *mut vk::PipelineInputAssemblyStateCreateInfo) };
    }
}

impl MyClone for *const vk::PipelineMultisampleStateCreateInfo {
    fn my_clone(&self) -> Self {
        if self.is_null() {
            return ptr::null();
        }
        let mut ret = Box::new(unsafe { self.read() });
        assert!(ret.p_next.is_null());
        ret.p_sample_mask =
            if ret.p_sample_mask.is_null() || ret.rasterization_samples.as_raw() == 0 {
                ptr::null()
            } else {
                Box::<[_]>::leak(
                    (*unsafe {
                        slice::from_raw_parts(
                            ret.p_sample_mask,
                            ret.rasterization_samples.as_raw() as usize / 32,
                        )
                    })
                    .into(),
                )
                .as_ptr()
            };
        Box::leak(ret)
    }

    fn my_drop(&self) {
        if self.is_null() {
            return;
        }
        let this = unsafe { Box::from_raw(*self as *mut vk::PipelineMultisampleStateCreateInfo) };
        if !this.p_sample_mask.is_null() && this.rasterization_samples.as_raw() != 0 {
            unsafe {
                Box::from_raw(ptr::from_raw_parts::<[vk::VertexInputBindingDescription]>(
                    this.p_sample_mask as *const (),
                    this.rasterization_samples.as_raw() as usize / 32,
                )
                    as *mut [vk::PipelineShaderStageCreateInfo]);
            }
        }
    }
}

impl MyClone for *const vk::PipelineDepthStencilStateCreateInfo {
    fn my_clone(&self) -> Self {
        if self.is_null() {
            return ptr::null();
        }
        let ret = Box::new(unsafe { self.read() });
        assert!(ret.p_next.is_null());
        Box::leak(ret)
    }

    fn my_drop(&self) {
        if self.is_null() {
            return;
        }
        unsafe { Box::from_raw(*self as *mut vk::PipelineDepthStencilStateCreateInfo) };
    }
}

impl MyClone for *const vk::PipelineColorBlendStateCreateInfo {
    fn my_clone(&self) -> Self {
        if self.is_null() {
            return ptr::null();
        }
        let mut ret = Box::new(unsafe { self.read() });
        assert!(ret.p_next.is_null());
        ret.p_attachments = if ret.p_attachments.is_null() || ret.attachment_count == 0 {
            ptr::null()
        } else {
            Box::<[_]>::leak(
                (*unsafe { slice::from_raw_parts(ret.p_attachments, ret.attachment_count as _) })
                    .into(),
            )
            .as_ptr()
        };
        Box::leak(ret)
    }

    fn my_drop(&self) {
        if self.is_null() {
            return;
        }
        let this = unsafe { Box::from_raw(*self as *mut vk::PipelineColorBlendStateCreateInfo) };
        if !this.p_attachments.is_null() && this.attachment_count != 0 {
            unsafe {
                Box::from_raw(ptr::from_raw_parts::<[vk::VertexInputBindingDescription]>(
                    this.p_attachments as *const (),
                    this.attachment_count as _,
                )
                    as *mut [vk::PipelineShaderStageCreateInfo]);
            }
        }
    }
}

impl MyClone for *const vk::PipelineDynamicStateCreateInfo {
    fn my_clone(&self) -> Self {
        if self.is_null() {
            return ptr::null();
        }
        let mut ret = Box::new(unsafe { self.read() });
        assert!(ret.p_next.is_null());
        ret.p_dynamic_states = if ret.p_dynamic_states.is_null() || ret.dynamic_state_count == 0 {
            ptr::null()
        } else {
            Box::<[_]>::leak(
                (*unsafe {
                    slice::from_raw_parts(ret.p_dynamic_states, ret.dynamic_state_count as _)
                })
                .into(),
            )
            .as_ptr()
        };
        Box::leak(ret)
    }

    fn my_drop(&self) {
        if self.is_null() {
            return;
        }
        let this = unsafe { Box::from_raw(*self as *mut vk::PipelineDynamicStateCreateInfo) };
        if !this.p_dynamic_states.is_null() && this.dynamic_state_count != 0 {
            unsafe {
                Box::from_raw(ptr::from_raw_parts::<[vk::VertexInputBindingDescription]>(
                    this.p_dynamic_states as *const (),
                    this.dynamic_state_count as _,
                )
                    as *mut [vk::PipelineShaderStageCreateInfo]);
            }
        }
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
pub struct MyGraphicsPipelineCreateInfo(vk::GraphicsPipelineCreateInfo);
impl MyGraphicsPipelineCreateInfo {
    pub(crate) fn get_infos(&self) -> &[vk::GraphicsPipelineCreateInfo] {
        slice::from_ref(&self.0)
    }
}

unsafe impl Send for MyGraphicsPipelineCreateInfo {}

impl From<vk::GraphicsPipelineCreateInfo> for MyGraphicsPipelineCreateInfo {
    fn from(this: vk::GraphicsPipelineCreateInfo) -> Self {
        let mut ret = this;
        assert!(ret.p_next.is_null());
        ret.p_stages = ret.p_stages.my_slice_clone(ret.stage_count);
        ret.p_vertex_input_state = ret.p_vertex_input_state.my_clone();
        ret.p_input_assembly_state = ret.p_input_assembly_state.my_clone();
        ret.p_tessellation_state = ret.p_tessellation_state.my_clone();
        ret.p_viewport_state = ret.p_viewport_state.my_clone();
        ret.p_rasterization_state = ret.p_rasterization_state.my_clone();
        ret.p_multisample_state = ret.p_multisample_state.my_clone();
        ret.p_depth_stencil_state = ret.p_depth_stencil_state.my_clone();
        ret.p_color_blend_state = ret.p_color_blend_state.my_clone();
        ret.p_dynamic_state = ret.p_dynamic_state.my_clone();
        Self(ret)
    }
}

impl Drop for MyGraphicsPipelineCreateInfo {
    fn drop(&mut self) {
        self.0.p_stages.my_slice_drop(self.0.stage_count);
        self.0.p_vertex_input_state.my_drop();
        self.0.p_input_assembly_state.my_drop();
        self.0.p_tessellation_state.my_drop();
        self.0.p_viewport_state.my_drop();
        self.0.p_rasterization_state.my_drop();
        self.0.p_multisample_state.my_drop();
        self.0.p_depth_stencil_state.my_drop();
        self.0.p_color_blend_state.my_drop();
        self.0.p_dynamic_state.my_drop();
    }
}
