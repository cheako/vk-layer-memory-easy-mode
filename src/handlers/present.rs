// Copyright (C) 2022 Michael Mestnik <cheako@mikemestnik.net>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, Read},
    lazy::SyncLazy,
    sync::{
        mpsc::{self, Receiver},
        Mutex, RwLock,
    },
    time::{Duration, Instant},
};

use ash::vk;
use timeout_readwrite::TimeoutReader;

struct Queue {
    khr_swapchain_fn: vk::KhrSwapchainFn,
}

#[allow(clippy::type_complexity)]
static QUEUE: SyncLazy<RwLock<HashMap<vk::Queue, Queue>>> = SyncLazy::new(Default::default);

pub(crate) unsafe extern "system" fn get_device_queue(
    device: vk::Device,
    queue_family_index: u32,
    queue_index: u32,
    p_queue: *mut vk::Queue,
) {
    let global = super::DEVICE.read().unwrap();
    let object = global.get(&device).unwrap();
    let khr_swapchain_fn = object.khr_swapchain_fn.clone();
    let device = object.device.clone();

    *p_queue = device.get_device_queue(queue_family_index, queue_index);

    QUEUE
        .write()
        .unwrap()
        .insert(p_queue.read(), Queue { khr_swapchain_fn });
}

pub(crate) unsafe extern "system" fn queue_present(
    queue: vk::Queue,
    p_present_info: *const vk::PresentInfoKHR,
) -> vk::Result {
    let global = QUEUE.read().unwrap();
    let object = global.get(&queue).unwrap();
    let ret = (object.khr_swapchain_fn.queue_present_khr)(queue, p_present_info);

    let now = Instant::now();

    /// Log timestamp reference
    static mut FIRST: Option<Instant> = None;
    if FIRST.is_none() {
        FIRST = Some(now);
    }

    static mut TIMER: Option<Instant> = None;
    match TIMER {
        // Is past limit
        Some(ctr) if Duration::SECOND * 2 < now - ctr => {
            TIMER = Some(now);
            todo!("shuffle or something?");
        }
        None => {
            // Set on first time
            TIMER = Some(now);
        }
        _ => {}
    };
    ret
}
