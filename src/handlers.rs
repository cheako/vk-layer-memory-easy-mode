// Copyright (C) 2022 Michael Mestnik <cheako@mikemestnik.net>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#![warn(unsafe_op_in_unsafe_fn)]

use super::DEVICE;

pub(crate) mod command_buffer;
pub(crate) mod present;
pub(crate) mod types;

pub(super) fn create_device() {
    use std::thread;

    thread::spawn(|| {
        let path = "/tmp/cheako_memory_easy_mode_sock";
        let _ = std::fs::remove_file(path);
        let listener = std::os::unix::net::UnixListener::bind(path)?;

        // accept connections and process them, spawning a new thread for each one
        for stream in listener.incoming() {
            match stream {
                Ok(mut stream) => {
                    /* connection succeeded */
                    thread::spawn(move || {
                        use std::io::Read;

                        let mut c = [0u8; 1];
                        if let Ok(1) = stream.read(&mut c) {
                            match c[0] {
                                _ => {}
                            }
                        }
                    });
                }
                Err(_) => {
                    /* connection failed */
                    break;
                }
            }
        }
        Ok::<(), std::io::Error>(())
    });
}
