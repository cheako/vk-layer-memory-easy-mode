// Copyright (C) 2022 Michael Mestnik <cheako@mikemestnik.net>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#![feature(duration_constants)]
#![feature(hash_drain_filter)]
#![feature(if_let_guard)]
#![feature(is_some_with)]
#![feature(let_chains)]
#![allow(clippy::missing_safety_doc)]
#![feature(once_cell)]
#![feature(ptr_const_cast)]
#![feature(result_flattening)]
#![feature(result_into_ok_or_err)]
#![feature(result_option_inspect)]
#![feature(ptr_metadata)]

use std::collections::HashMap;
use std::ffi;
use std::lazy::SyncLazy;
use std::mem::MaybeUninit;
use std::panic::catch_unwind;
use std::sync::{Once, RwLock};

use ash::vk;

mod types;
use types::*;

mod handlers;

macro_rules! link_instance_lookup {
    ($link:ident, $instance:expr, $cname:expr, $let:ident ) => {
        let cname = ffi::CStr::from_bytes_with_nul_unchecked($cname);
        let $let = ($link.pfn_next_get_instance_proc_addr)($instance, cname.as_ptr()).unwrap()
            as *const ffi::c_void;
    };
}

macro_rules! link_device_lookup {
    ($link:ident, $device:expr, $cname:expr, $let:ident ) => {
        let cname = ffi::CStr::from_bytes_with_nul_unchecked($cname);
        let $let = ($link.pfn_next_get_device_proc_addr)($device, cname.as_ptr()).unwrap()
            as *const ffi::c_void;
    };
}

static mut ENTRY: MaybeUninit<vk::StaticFn> = MaybeUninit::uninit();
static mut ENTRY_INIT: Once = Once::new();

#[allow(clippy::type_complexity)]
static INSTANCE: SyncLazy<RwLock<HashMap<vk::Instance, ash::Instance>>> =
    SyncLazy::new(Default::default);

#[allow(clippy::type_complexity)]
static PHYSICAL_DEVICES: SyncLazy<RwLock<HashMap<vk::PhysicalDevice, vk::Instance>>> =
    SyncLazy::new(Default::default);

struct Device {
    khr_swapchain_fn: vk::KhrSwapchainFn,
    device: ash::Device,
    instance_fn: vk::InstanceFnV1_0,
}

#[allow(clippy::type_complexity)]
static DEVICE: SyncLazy<RwLock<HashMap<vk::Device, Device>>> = SyncLazy::new(Default::default);

fn enable_debug() {
    if false {
        std::fs::write("debug/tracing/events/amdgpu/amdgpu_bo_move/enable", "1\n")
            .expect("Unable to write 1 to enable file");
    }
}

unsafe extern "system" fn create_instance(
    p_create_info: *const vk::InstanceCreateInfo,
    p_allocator: *const vk::AllocationCallbacks,
    p_instance: *mut vk::Instance,
) -> vk::Result {
    let result = catch_unwind(|| {
        enable_debug();

        let instance_info = p_create_info.as_ref().unwrap();

        let mut layer_info: *const VkLayerInstanceCreateInfo = instance_info.p_next.cast();
        while !layer_info.is_null()
            && (layer_info.read().s_type != vk::StructureType::LOADER_INSTANCE_CREATE_INFO
                || layer_info.read().function != VkLayerFunction::VALUE)
        {
            layer_info = layer_info.read().p_next.cast::<VkLayerInstanceCreateInfo>();
        }
        let inner_layer_info = layer_info.read().p_layer_info.read();
        (*layer_info.as_mut()).p_layer_info = inner_layer_info.p_next;

        let cname = ffi::CStr::from_bytes_with_nul_unchecked(b"vkCreateInstance\0");
        let create_instance = std::mem::transmute::<_, vk::PFN_vkCreateInstance>(
            (inner_layer_info.pfn_next_get_instance_proc_addr)(
                vk::Instance::null(),
                cname.as_ptr(),
            )
            .unwrap(),
        );
        let ret = create_instance(p_create_info, p_allocator, p_instance);
        let instance = p_instance.read();

        link_instance_lookup!(
            inner_layer_info,
            p_instance.read(),
            b"vkGetInstanceProcAddr\0",
            get_instance_proc_addr
        );
        ENTRY_INIT.call_once(|| {
            ENTRY = MaybeUninit::new(vk::StaticFn::load(move |name: &ffi::CStr| {
                match name.to_bytes() {
                    b"vkGetInstanceProcAddr" => get_instance_proc_addr,
                    _ => panic!("Missing entry proc addr: {:?}", name),
                }
            }));
        });

        INSTANCE.write().unwrap().insert(
            instance,
            ash::Instance::load(
                &vk::StaticFn::load(move |name: &ffi::CStr| match name.to_bytes() {
                    b"vkGetInstanceProcAddr" => get_instance_proc_addr,
                    _ => panic!("Missing entry proc addr: {:?}", name),
                }),
                instance,
            ),
        );
        ret
    });
    assert!(result.is_ok());
    result.unwrap()
}

unsafe extern "system" fn enumerate_physical_devices(
    instance: vk::Instance,
    p_physical_device_count: *mut u32,
    p_physical_devices: *mut vk::PhysicalDevice,
) -> vk::Result {
    let result = catch_unwind(|| {
        let ret = (INSTANCE
            .read()
            .unwrap()
            .get(&instance)
            .unwrap()
            .fp_v1_0()
            .enumerate_physical_devices)(
            instance, p_physical_device_count, p_physical_devices
        );

        if p_physical_devices.is_null() {
            return ret;
        }

        if let Ok::<isize,_>(physical_device_count) = p_physical_device_count.read().try_into()
              &&  physical_device_count > 0 {
            let mut physical_devices = PHYSICAL_DEVICES.write().unwrap();
            for count in 0..physical_device_count {
                physical_devices.insert(
                    p_physical_devices.offset(count).read(),
                    instance,
                );
            }
        }

        ret
    });
    assert!(result.is_ok());
    result.unwrap()
}

unsafe extern "system" fn create_device(
    physical_device: vk::PhysicalDevice,
    p_create_info: *const vk::DeviceCreateInfo,
    p_allocator: *const vk::AllocationCallbacks,
    p_device: *mut vk::Device,
) -> vk::Result {
    let result = catch_unwind(|| {
        let device_info = p_create_info.as_ref().unwrap();

        let mut layer_info = device_info.p_next.cast::<VkLayerDeviceCreateInfo>();
        while !layer_info.is_null()
            && (layer_info.read().s_type != vk::StructureType::LOADER_DEVICE_CREATE_INFO
                || layer_info.read().function != VkLayerFunction::VALUE)
        {
            layer_info = layer_info.read().p_next.cast::<VkLayerDeviceCreateInfo>();
        }
        let inner_layer_info = layer_info.read().p_layer_info.read();
        (*layer_info.as_mut()).p_layer_info = inner_layer_info.p_next;

        let global = PHYSICAL_DEVICES.read().unwrap();
        let instance = *global.get(&physical_device).unwrap();
        let cname = ffi::CStr::from_bytes_with_nul_unchecked(b"vkCreateDevice\0");
        let create_device = std::mem::transmute::<_, vk::PFN_vkCreateDevice>(
            (inner_layer_info.pfn_next_get_instance_proc_addr)(instance, cname.as_ptr()).unwrap(),
        );
        let ret = create_device(physical_device, p_create_info, p_allocator, p_device);

        let device = p_device.read();
        link_instance_lookup!(
            inner_layer_info,
            instance,
            b"vkGetInstanceProcAddr\0",
            get_instance_proc_addr
        );
        link_instance_lookup!(
            inner_layer_info,
            instance,
            b"vkDestroyInstance\0",
            destroy_instance
        );
        link_instance_lookup!(
            inner_layer_info,
            instance,
            b"vkEnumeratePhysicalDevices\0",
            enumerate_physical_devices
        );
        link_instance_lookup!(
            inner_layer_info,
            instance,
            b"vkGetPhysicalDeviceFeatures\0",
            get_physical_device_features
        );
        link_instance_lookup!(
            inner_layer_info,
            instance,
            b"vkGetPhysicalDeviceFormatProperties\0",
            get_physical_device_format_properties
        );
        link_instance_lookup!(
            inner_layer_info,
            instance,
            b"vkGetPhysicalDeviceImageFormatProperties\0",
            get_physical_device_image_format_properties
        );
        link_instance_lookup!(
            inner_layer_info,
            instance,
            b"vkGetPhysicalDeviceProperties\0",
            get_physical_device_properties
        );
        link_instance_lookup!(
            inner_layer_info,
            instance,
            b"vkGetPhysicalDeviceQueueFamilyProperties\0",
            get_physical_device_queue_family_properties
        );
        link_instance_lookup!(
            inner_layer_info,
            instance,
            b"vkGetPhysicalDeviceMemoryProperties\0",
            get_physical_device_memory_properties
        );
        link_device_lookup!(
            inner_layer_info,
            device,
            b"vkGetDeviceProcAddr\0",
            get_device_proc_addr
        );
        link_instance_lookup!(
            inner_layer_info,
            instance,
            b"vkCreateDevice\0",
            create_device
        );
        link_instance_lookup!(
            inner_layer_info,
            instance,
            b"vkEnumerateDeviceExtensionProperties\0",
            enumerate_device_extension_properties
        );
        link_instance_lookup!(
            inner_layer_info,
            instance,
            b"vkEnumerateDeviceLayerProperties\0",
            enumerate_device_layer_properties
        );
        link_instance_lookup!(
            inner_layer_info,
            instance,
            b"vkGetPhysicalDeviceSparseImageFormatProperties\0",
            get_physical_device_sparse_image_format_properties
        );
        link_device_lookup!(
            inner_layer_info,
            device,
            b"vkCreateSwapchainKHR\0",
            create_swapchain_khr
        );
        link_device_lookup!(
            inner_layer_info,
            device,
            b"vkDestroySwapchainKHR\0",
            destroy_swapchain_khr
        );
        link_device_lookup!(
            inner_layer_info,
            device,
            b"vkGetSwapchainImagesKHR\0",
            get_swapchain_images_khr
        );
        link_device_lookup!(
            inner_layer_info,
            device,
            b"vkAcquireNextImageKHR\0",
            acquire_next_image_khr
        );
        link_device_lookup!(
            inner_layer_info,
            device,
            b"vkQueuePresentKHR\0",
            queue_present_khr
        );
        link_device_lookup!(
            inner_layer_info,
            device,
            b"vkGetDeviceGroupPresentCapabilitiesKHR\0",
            get_device_group_present_capabilities_khr
        );
        link_device_lookup!(
            inner_layer_info,
            device,
            b"vkGetDeviceGroupSurfacePresentModesKHR\0",
            get_device_group_surface_present_modes_khr
        );
        link_device_lookup!(
            inner_layer_info,
            device,
            b"vkGetDeviceGroupSurfacePresentModesKHR\0",
            get_physical_device_present_rectangles_khr
        );
        link_device_lookup!(
            inner_layer_info,
            device,
            b"vkAcquireNextImage2KHR\0",
            acquire_next_image2_khr
        );
        #[rustfmt::skip]
        macro_rules! load_proc {
            ($match_name:ident, $end:block) => {
                match $match_name.to_bytes() {
                    b"vkGetInstanceProcAddr"                          => get_instance_proc_addr,
                    b"vkDestroyInstance"                              => destroy_instance,
                    b"vkEnumeratePhysicalDevices"                     => enumerate_physical_devices,
                    b"vkGetPhysicalDeviceFeatures"                    => get_physical_device_features,
                    b"vkGetPhysicalDeviceFormatProperties"            => get_physical_device_format_properties,
                    b"vkGetPhysicalDeviceImageFormatProperties"       => get_physical_device_image_format_properties,
                    b"vkGetPhysicalDeviceProperties"                  => get_physical_device_properties,
                    b"vkGetPhysicalDeviceQueueFamilyProperties"       => get_physical_device_queue_family_properties,
                    b"vkGetPhysicalDeviceMemoryProperties"            => get_physical_device_memory_properties,
                    b"vkGetDeviceProcAddr"                            => get_device_proc_addr,
                    b"vkCreateDevice"                                 => create_device,
                    b"vkEnumerateDeviceExtensionProperties"           => enumerate_device_extension_properties,
                    b"vkEnumerateDeviceLayerProperties"               => enumerate_device_layer_properties,
                    b"vkGetPhysicalDeviceSparseImageFormatProperties" => get_physical_device_sparse_image_format_properties,
                    b"vkCreateSwapchainKHR"                           => create_swapchain_khr,
                    b"vkDestroySwapchainKHR"                          => destroy_swapchain_khr,
                    b"vkGetSwapchainImagesKHR"                        => get_swapchain_images_khr,
                    b"vkAcquireNextImageKHR"                          => acquire_next_image_khr,
                    b"vkQueuePresentKHR"                              => queue_present_khr,
                    b"vkGetDeviceGroupPresentCapabilitiesKHR"         => get_device_group_present_capabilities_khr,
                    b"vkGetDeviceGroupSurfacePresentModesKHR"         => get_device_group_surface_present_modes_khr,
                    b"vkGetPhysicalDevicePresentRectanglesKHR"        => get_physical_device_present_rectangles_khr,
                    b"vkAcquireNextImage2KHR"                         => acquire_next_image2_khr,
                    _ => $end,
                }
            };
        }
        let instance = vk::InstanceFnV1_0::load(move |name: &ffi::CStr| {
            load_proc!(name, { panic!("Missing instance proc addr: {:?}", name) })
        });
        let device = ash::Device::load(
            &vk::InstanceFnV1_0::load(move |name: &ffi::CStr| {
                load_proc!(name, { panic!("Missing device proc addr: {:?}", name) })
            }),
            device,
        );
        let khr_swapchain_fn = vk::KhrSwapchainFn::load(move |name: &ffi::CStr| {
            load_proc!(name, { panic!("Missing swapchain proc addr: {:?}", name) })
        });
        DEVICE.write().unwrap().insert(
            device.handle(),
            Device {
                instance_fn: instance,
                device,
                khr_swapchain_fn,
            },
        );
        handlers::create_device();
        ret
    });
    assert!(result.is_ok());
    result.unwrap()
}

macro_rules! to_void {
    ($name:ident, $pfn:ident) => {
        std::mem::transmute::<_, vk::PFN_vkVoidFunction>($name as vk::$pfn)
    };
}

#[rustfmt::skip]
macro_rules! get_device_proc_match {
    ($match_name:ident, $end:block) => {
        match $match_name.to_bytes() {
            b"vkAllocateCommandBuffers"  => to_void!(allocate_command_buffers,      PFN_vkAllocateCommandBuffers),
            b"vkQueuePresentKHR"         => to_void!(queue_present,                 PFN_vkQueuePresentKHR),
            b"vkGetDeviceQueue"          => to_void!(get_device_queue,              PFN_vkGetDeviceQueue),
            b"vkGetDeviceProcAddr"       => to_void!(CheakoMemoryEasyMode_GetDeviceProcAddr, PFN_vkGetDeviceProcAddr),
            _ => $end,
        }
    };
}

#[no_mangle]
pub unsafe extern "system" fn CheakoMemoryEasyMode_GetDeviceProcAddr(
    device: vk::Device,
    p_name: *const std::os::raw::c_char,
) -> vk::PFN_vkVoidFunction {
    let result = catch_unwind(|| {
        use handlers::{command_buffer::*, present::*};
        let name = ffi::CStr::from_ptr(p_name);
        get_device_proc_match!(name, {
            (DEVICE
                .read()
                .unwrap()
                .get(&device)
                .unwrap()
                .instance_fn
                .get_device_proc_addr)(device, p_name)
        })
    });
    assert!(result.is_ok());
    result.unwrap()
}

#[rustfmt::skip]
macro_rules! get_instance_proc_match {
    ($match_name:ident, $end:block) => {
        get_device_proc_match!($match_name, {
            match $match_name.to_bytes() {
                b"vkCreateInstance"           => to_void!(create_instance,                 PFN_vkCreateInstance),
                b"vkEnumeratePhysicalDevices" => to_void!(enumerate_physical_devices,      PFN_vkEnumeratePhysicalDevices),
                b"vkCreateDevice"             => to_void!(create_device,                   PFN_vkCreateDevice),
                b"vkGetInstanceProcAddr"      => to_void!(CheakoMemoryEasyMode_GetInstanceProcAddr, PFN_vkGetInstanceProcAddr),
                _ => $end,
            }
        })
    }
}

#[no_mangle]
pub unsafe extern "system" fn CheakoMemoryEasyMode_GetInstanceProcAddr(
    instance: vk::Instance,
    p_name: *const std::os::raw::c_char,
) -> vk::PFN_vkVoidFunction {
    let result = catch_unwind(|| {
        use handlers::{command_buffer::*, present::*};
        let name = ffi::CStr::from_ptr(p_name);
        get_instance_proc_match!(name, {
            assert!(ENTRY_INIT.is_completed());
            (ENTRY.assume_init_ref().get_instance_proc_addr)(instance, p_name)
        })
    });
    assert!(result.is_ok());
    result.unwrap()
}
